import {upsert} from "@/helpers"

export default {
    setItem(state, {ressource, item}){
        upsert(state[ressource].items, item)
    },

    appendUnsubscribe(state, unsubscribe){
        state.unsubscribes.push(unsubscribe)
    },

    clearState(state){
        state.unsubscribes = []
    },

    

}