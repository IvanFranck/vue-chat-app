import {createStore} from "vuex"

import auth from "./modules/auth"
import users from "./modules/users"
import chats from "./modules/chats"
import messages from "./modules/messages"
import actions from "./actions"
import mutations from "./mutations"


export default createStore({
    modules: {
        auth,
        users,
        chats,
        messages
    },
    state: {
        unsubscribes: []
    },
    actions,
    mutations,
})