import { getDatabase, onValue, ref } from "firebase/database"
import {snapToData} from "@/helpers"

export default{

    /**
     * fetch item in firebase and save it in a store state (ressource)
     * 
     * @param id :  the id of item.
     * @param ressource :  the state of store where the item well be save.
     * @param dbPath : the path in firebase where the item is save.
     * @param handleUnsubscribe: the function for remove listenner. the default value is null.
     * @param once: set it to true if you want to remove automaticaly the listener after retrieve the item in firebase.
     *  
     * @returns resolver with item if it exist or null if not
     */

    fetchItem({commit}, {ressource, dbPath, handleUnsubscribe = null, once = false, store = true}){
        return new Promise((resolve)=>{
            const db = getDatabase()
            const ressourceRef = ref(db, dbPath)
            const unSubscribe = onValue(ressourceRef, async snap=>{
                let item = {}
                if (snap.exists()){
                    if (snap.hasChildren()) {
                        item = await snapToData(snap)
                        item['id'] = snap.key
                        if (store) commit('setItem', {ressource, item})
                    }else{
                        item[snap.key] = snap.val()
                    }
                    resolve(item)
                }else{
                    resolve(null)
                }
                
            }, {
                onlyOnce: once
            }) 
            if(handleUnsubscribe){
                handleUnsubscribe(unSubscribe)
            }else{
                commit('appendUnsubscribe', unSubscribe)
            }
        })
    },

    fetchItems({dispatch}, {ids, ressource, dbPath, once}){
        ids = ids || []
        console.log('ids: ', ids);
        return Promise.all(ids.map(id=> dispatch('fetchItem', {ressource, dbPath: `${dbPath+id}`, once })))
    },

    async unSubscribeAll({state, commit}){
        state.unsubscribes.forEach(async unSubscribe => {
            await unSubscribe()
        });
        commit('clearState')
    },

    async clearStore({commit}, {ressources}){
        if (typeof ressources === 'object') {
            ressources.forEach(async ressource=>{
                await commit(ressource+'/clearState')
            })
        }else await commit(ressources+'/clearState')
    }

}