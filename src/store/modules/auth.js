import {getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, updateProfile} from "firebase/auth"
import { getDatabase, onValue, ref } from "firebase/database";
import {snapToData} from "@/helpers"
export default {
    namespaced: true,

    state:{
        authId: null,
        authObserverUnsubscribe: null,
        authUserUnsubscribe: false
    },

    mutations: {
        setAuthId(state, id){
            state.authId = id
        },

        async clearState(state){
            console.log("clear auth state ");
            state.authId = null

        },

        setAuthObserverUnsubscribe(state, unsubscribe){
            state.authObserverUnsubscribe = unsubscribe
        },

        setAuthUserUnsubscribe(state){
            console.log("auth user unsubscribe")
            state.authUserUnsubscribe = true
        }


    },

    actions:{
        async registerUserWithEmailAndPassword({dispatch, commit}, {email,password,username}){
            const auth = getAuth();
            let userId = null
            await createUserWithEmailAndPassword(auth, email, password).then( credentials=>{
                userId = credentials.user.uid
                commit('setAuthId', userId)
            })
            await updateProfile(auth.currentUser, {
                displayName: username
            })
            await dispatch('users/createUser', {uid: userId, email, username}, {root: true})
        },

        async signInWithEmailAndPassword(context,{email, password}){
            
            const auth = getAuth();
            await signInWithEmailAndPassword(auth, email, password)
        },

        async signOut ({ commit }) {
            const auth = getAuth()
            await auth.signOut()
      
            commit('setAuthId', null)
        },

        initAuthentification({commit, state}){
            if (state.authObserverUnsubscribe) state.authObserverUnsubscribe()
            return new Promise((resolve)=>{
                const auth = getAuth();
                const db = getDatabase()
                const unsubscribe = onAuthStateChanged(auth,  (user)=>{
                    if (user && !state.authUserUnsubscribe){ 
                        console.log('auth state change');
                        commit('setAuthId', user.uid)
                        //fetch auth user infos
                        onValue(ref(db, 'users/'+user.uid), async (snap)=>{
                            const authUser = await snapToData(snap)
                            commit('users/setAuthUser', authUser, {root: true})
                        })
                        resolve(user)

                    }
                    else{
                        commit('setAuthId', null)
                        resolve(null)
                    }
                })
                commit('setAuthObserverUnsubscribe', unsubscribe)

            })
        },

        fecthAuthUser(){
            console.log("fetch auth user");
            return new Promise(resolve=>{
                const db = getDatabase()
                const authUserRef = ref(db, '/users/'+getAuth().currentUser.uid)
                const unsubscribe = onValue(authUserRef, snap=>{
                    const user = {}
                    snap.forEach(item=>{
                        user[item.key] = item.val()
                    })
                    resolve(user)
                })
                unsubscribe()
            })
        }
    }

}