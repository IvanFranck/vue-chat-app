import { getAuth } from "firebase/auth";
import { onChildAdded } from "firebase/database"
import {
  child,
  getDatabase,
  push,
  ref,
  serverTimestamp,
  update,
} from "firebase/database";

export default {
  namespaced: true,

  state: {
    items: [],
    unSubscribeNewMessageListenner: null

  },

  getters: {},

  mutations: {
    resetMessages(state) {
        console.log("message reseted");
      state.items = [];
    },

    setUnSubscribeNewMessageListenner(state, listenner){
        console.log("new message listenner unsubscribe");
        state.unSubscribeNewMessageListenner = listenner
    },

    async clearState(state){
      console.log("clear messages state ");
      state.unSubscribeNewMessageListenner = null
      state.items = []
    } 
  },

  actions: {
    async createMessage(context, { message, selectedChat }) {
      const chat = { ...selectedChat };

      const db = getDatabase();
      const authUser = getAuth().currentUser;

      // ****** save message to firebase
      const newMessageRef = push(child(ref(db), "messages")).key;
      const newMessageData = {
        message,
        createdAt: serverTimestamp(),
        author: authUser.uid,
        chatId: chat.id,
      };

      // ****** append message to & update chat's property (last message & updatedAt)
      const chatId = selectedChat.id;
      delete chat.id;
      chat.updatedAt = serverTimestamp();
      chat.lastMessage = message;
      if ("messages" in chat) {
        chat.messages[newMessageRef] = serverTimestamp();
      } else {
        chat["messages"] = {};
        chat.messages[newMessageRef] = serverTimestamp();
      }

      // Write the new message's data and add it key simultaneously in chat list.

      const updates = {};
      updates["/messages/" + newMessageRef] = newMessageData;
      updates["/chats/" + chatId] = chat;
      update(ref(db), updates)
        .then((result) => {
          console.log("result: ", result);
        })
        .catch((error) => {
          console.error("error: ", error);
        });
    },

    fetchChatMessages({ dispatch, commit }, { chat }) {
      const chatData = { ...chat };
      commit("resetMessages");
      if ("messages" in chatData) {
        return new Promise((resolve) => {
          dispatch(
            "fetchItems",
            {
              ids: Object.keys(chatData.messages),
              dbPath: "/messages/",
              ressource: "messages",
              once: true,
            },
            { root: true }
          ).then((result) => {
            resolve(result);
            console.log("all messages fetched", result);
          });

          //add listener on chat's message data
          const db = getDatabase()
          const messageListRef = ref(db, 'chats/'+chat.id+'/messages')
          const listenner = onChildAdded(messageListRef, async (data)=>{
              await dispatch("fetchItem", {
                  ressource: 'messages',
                  dbPath: '/messages/'+data.key,
                  once: true
              }, {root: true})
          })

          commit('setUnSubscribeNewMessageListenner', listenner)
        });
      }
    },

  },
};
