import { getAuth } from "firebase/auth"
import { getDatabase, ref, push, serverTimestamp, child, update, onChildAdded } from "firebase/database";
import { set } from "lodash";


export default {
    namespaced: true,

    state:{
        items: [],
        selectedChatId: 0,
        unSubscribeNewMessageListenner: null
    },

    getters: {

    },

    mutations: {
        setSelectedChatId(state, id){
            state.selectedChatId = id
        },
        setUnSubscribeNewMessageListenner(state, listenner){
            console.log("new message listenner unsubscribe");
            state.unSubscribeNewMessageListenner = listenner
        },

        async clearState(state){
            console.log("clear chats state ");
            state.items = []
            state.unSubscribeNewMessageListenner = null
            state.selectedChatId = 0

        }
    },

    actions:{

        async createChat({rootState, dispatch, commit}, friendId){

                const authUser = getAuth().currentUser
                const db = getDatabase()
                
                /**
                 * add user id in friend list if he wasn't already there 
                 */

                const friendListRef = ref(db, 'users/'+authUser.uid+'/friends')
                let user = rootState['users/authUser']?.friends.find(item=> item == friendId)
                console.log('user: ', user);
                if (user) {
                    console.log("this user is already a friend");
                    return {
                        type: 'error',
                        message: 'this user is already a friend'
                    }
                    
                }
                // fetch friend infos
                let friendInfos = null
                await dispatch('fetchItem', {
                    id: friendId,
                    ressource: 'users',
                    dbPath: 'users/'+friendId,
                    once: true,
                    store: false
                },
                {root: true}
                ).then(item=>{
                    console.log('item: ', item);
                    friendInfos = item
                })
                if (!friendInfos) {
                    return {
                        type: "error",
                        message: "This user doesn't exit"
                    }
                    
                }
                const newFriendRef = push(friendListRef)
                const data = {}
                data[friendInfos?.username] = friendId
                set(newFriendRef, data)



                /**
                 * create new chat and add created chat in both users (current & new friend) chatList
                 */

    
                const newChatData = {
                    lastMessage: '',
                    messages: {},
                    createdAt: serverTimestamp(),
                    updatedAt: serverTimestamp(),
                    members: {}
                }
                newChatData.members[authUser.displayName] = authUser.uid
                newChatData.members[friendInfos.username] = friendId

                // Get a key for a new chat.
                const newChatKey = push(child(ref(db), 'chats')).key 

                // Write the new chat's data in the chat list and it key simultaneously in users's chat list.
                const updates = {}
                updates['/chats/'+newChatKey] = newChatData
                updates['/users/'+authUser.uid+'/chats/'+newChatKey] = serverTimestamp()
                updates['/users/'+friendId+'/chats/'+newChatKey] = serverTimestamp()
                
                update(ref(db), updates)

                //add listener on chat's message data
                const messageListRef = ref(db, 'chats/'+newChatKey+'/messages')
                const listenner = onChildAdded(messageListRef, async (data)=>{
                    await dispatch("fetchItem", {
                        ressource: 'messages',
                        dbPath: '/messages/'+data.key,
                        once: true
                    }, {root: true})
                })

                commit('messages/setUnSubscribeNewMessageListenner', listenner, {root: true})

    
                return{
                    type: "info",
                    message: "chat crated succesfully"
                }
            
        },

        async fetchUserChats({ dispatch, state}){

            const authUser = await dispatch('auth/fecthAuthUser',null, {root: true})
            if (state.unSubscribeNewMessageListenner) state.unSubscribeNewMessageListenner()
            return new Promise ((resolve)=>{
                if('chats' in authUser){
                    console.log("auth user have chats");
                    const chatKeys = Object.keys(authUser.chats)
                    const chats = dispatch("fetchItems", {
                        ids: chatKeys,
                        ressource: 'chats',
                        dbPath: '/chats/',
                        // once: true
                    }, 
                    {root: true}
                    )

                    resolve(chats)
                }else{
                    console.log("auth user have not chats");
                    resolve(null)
                }

            })
        }
    }
}