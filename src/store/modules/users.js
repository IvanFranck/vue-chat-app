import { getAuth } from "firebase/auth"
import { getDatabase, ref, serverTimestamp, onValue,update } from "firebase/database"

export default {
    namespaced: true,

    state:{
        authUser: null,
    },

    

    mutations: {
        setAuthUserFriends (state, user){
            const userId = state.friends.find(elt=>{
                elt.uid = user.uid
            })
            if(!userId) state.friends.push(user)
        },

        setAuthUser (state, user){
            state.authUser = user;
        },

        async clearState(state){
            console.log("clear users state ");
            state.authUser = null
        } 
        
    },

    actions:{
        async createUser(context, {email, username, uid}){
            const db = getDatabase()
            // const newUserkey = push(child(ref(db), 'users')).key
            const data = {}
            data['/users/' + uid] = {
                email,
                username,
                uid,
                createdAt: serverTimestamp()
            }
            update(ref(db), data)
        },

        async fetchUser(context, userId){
            return new Promise((resolve)=>{

                const db = getDatabase()
                const userRef = ref(db, 'users/'+userId)
                let val = null
                onValue(userRef, async snapshot=>{
                    if (snapshot.exists()){
                        console.log('snapshot.val(): ', snapshot.val());
                        val = await snapshot.val()
                    }
                })
                resolve(val)
            })
        }
    }
}