import { createRouter, createWebHistory } from "vue-router";
import store from "@/store"

const routes = [
  {
    path: "/signup",
    name: "SignUp",
    component: () => import("@/pages/SignUp.vue"),
  },
  {
    path: "/",
    name: "LogIn",
    component: () => import("@/pages/LogIn.vue"),
  },
  {
    path: "/app",
    name: "ChatApp",
    component: () => import("@/pages/ChatPage.vue"),
    meta: {requireAuth: true},
  },
];



const router = createRouter({
  history: createWebHistory(),
  routes,
})

router.beforeEach(async(to, from)=>{
  let authId = null
  console.log("before each router guard");
  await store.dispatch("auth/initAuthentification").then(user=>{
    if(user) authId = user.uid
  })


  if(to.meta.requireAuth && !authId) return { name: "LogIn", query: {redirectTo: to.path} }

  
})



export default router
