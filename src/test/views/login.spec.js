import {test, expect, describe} from "vitest"
import {mount, RouterLinkStub} from "@vue/test-utils"

import Login from "@/pages/LogIn.vue"
const wrapper = mount(Login, {
    global: {
        stubs: {
            RouterLink: RouterLinkStub,
        }
    }
})

describe("login.vue", ()=>{
    test("should show the form ", ()=>{
        expect(wrapper.find('form').exists()).toBe(true)
    })

    test('should form have two input fields', ()=>{
        expect(wrapper.findAll('input').length).toEqual(2)
    })

    test("should form have email input field", ()=>{
        expect(wrapper.find('input[type="email"]').exists()).toBe(true)
    })

    test("should form have password input field", ()=>{
        expect(wrapper.find('input[type="password"]').exists()).toBe(true)
    })

    test("should show link to go to create account view", ()=>{
        const routerLink = wrapper.findComponent(RouterLinkStub)
        expect(routerLink.props().to).toBe('/signup')
        expect(routerLink.text()).toBe('Create one')
    })
})

