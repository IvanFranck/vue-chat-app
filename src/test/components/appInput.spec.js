import {mount} from "@vue/test-utils"
import {describe, test, expect} from "vitest"
import AppInput from "@/components/AppInput.vue"

const wrapper = mount(AppInput)

describe('appInpit_component test', ()=>{

    test("should show input field", ()=>{
        expect(wrapper.find('input').exists()).toBe(true)
    })

    describe("props test", async()=>{

        await wrapper.setProps(
            {
                label: "hello world",
                placeholder: "type something",
                inputType: "password",
            }
        )
        test("AppInput should display label text", ()=>{

            expect(wrapper.get('label').text()).toContain('hello world')
        })

        test("input field should have placeholder", ()=>{
            expect(wrapper.find('input[placeholder="type something"]').exists()).toBe(true)
        })

        test("should input field be the right type", async()=>{
            expect(wrapper.find('input[type="password"]').exists()).toBe(true)
            await wrapper.setProps({inputType: 'email'})
            expect(wrapper.find('input[type="email"]').exists()).toBe(true)
        })
    })
})