import dayjs from "dayjs"

export function findById(ressources, id){
  let item = null
  
  item = ressources.find(item=>{
    if ('uid' in item) return item.uid === id
    if ('id' in item) return item.id === id
  })
  return item

}

export const upsert = (resources, resource) => {
    const index = resources.findIndex(p => p.id === resource.id)
    if (resource.id && index !== -1) {
      resources[index] = resource
    } else {
      resources.push(resource)
    }
}

export const snapToData = async(snapshot) => {
  const data = {}
  snapshot.forEach(child => {
    data[child.key] = child.val()
  });

  return data;
}

export const timestampToDate = (timestamp, format) => {
 return dayjs('2019-01-25').format(format)
}