import { createApp } from 'vue'
import App from './App.vue'
import firebaseConfig from "@/config/firebase"
import {initializeApp} from "firebase/app"
import store from "@/store"
import router from "@/router"


// Initialize Firebase
initializeApp(firebaseConfig)

import './assets/main.css'

const vueChatApp = createApp(App)

vueChatApp.use(store)
vueChatApp.use(router)

vueChatApp.mount('#app')
