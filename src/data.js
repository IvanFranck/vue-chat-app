export default {
    "users": {
        "12JIZIA2Q": {
            "email": "ivan@gmail.com",
            "createdAt": 129728277897955,
            "username": "ivanFranck",
            "uid": "12JIZIA2Q",
            "friends": {
                "johnDoe": "AAZ33PO24"
            },
            "chats": {
                "world": 129728277897955, //timestamp chat last update
                "12JIZIA2Q<->AAZ33PO24": 129728277897955
            }
        },
        "AAZ33PO24": {
            "email": "johnDoe@gmail.com",
            "createdAt": 134039273098455,
            "username": "johnDoe",
            "uid": "AAZ33PO24",
            "chats": {
                "world": true,
                "12JIZIA2Q<->AAZ33PO24": true
            }
        },
        "KJS3340D34": {
            "email": "cedirc@gmail.com",
            "createdAt": 134203434544556,
            "username": "cedric",
            "uid": "KJS3340D34",
            "chats": {
                
                "world": 1762893890937987 
            }
        }
    },
    "chats":{
        "world": {
            "lastMessage": "Ea quis quis duis veniam sint sunt nulla culpa duis nulla occaecat adipisicing.",
            "createdAt": 1762893890937987,
            "updatedAt": 1762893890937987, //change when new message is added
            "members": {
                "ivanFranck": "12JIZIA2Q",
                "jonhDoe": "AAZ33PO24",
            },
            "messages": {
                "m1": "12JIZIA2Q"
            }
        },
        "12JIZIA2Q<->AAZ33PO24":{
            "lastMessage": "Ea quis quis duis veniam sint sunt nulla culpa duis nulla occaecat adipisicing.",
            "createdAt": 1762893890937987,
            "updatedAt": 1762893890937987,
            "members": {
                "ivanFranck": "12JIZIA2Q",
                "jonhDoe": "AAZ33PO24",
            },
            "messages": {
                "m4": "12JIZIA2Q" // author id
            }
        }
    },

    "messages": {
        "m1":{
            "message": "Labore ad aute ullamco velit exercitation aute qui occaecat amet elit.",
            "createdAt": 1762893890937987,
            "author": "12JIZIA2Q" // user id
        },
        "m2": {},
        "m3": {},
        "m4": {}
    }
}