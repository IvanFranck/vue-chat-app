import AppInput from './AppInput.vue'

const options = {
  label: 'Email', 
  placeholder:"nzima.ivan.franck@gmail.com",
  inputType: 'email',
} 



describe('<AppInput />', () => {

  it('renders', () => {
    cy.mount(AppInput, {props:{inputType: 'text'}})
  })

  it('should have input field', ()=>{
    cy.mount(AppInput, {props:{inputType: 'text'}})
    cy.get(".form-group").find('input')
  })

  
  it('should component receives props', ()=>{
    cy.mount(AppInput, {
      props: {...options}
    })

    cy.get('label').should('have.text', options.label)
    cy.get('input').should('have.attr', 'placeholder', options.placeholder)
    cy.get('input').should('have.attr', 'type', options.inputType)
  })

  it('input event should emit', ()=>{
    const onInputSpy = cy.spy().as('onInputSpy')
    cy.mount(AppInput, {props: {...options, 'handleTyping': onInputSpy}})
    cy.get('input').type('hello')
    // cy.log(cy.spy())
    cy.get('@onInputSpy').should('be.called')
    
  })
})