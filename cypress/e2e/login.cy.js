
describe('Login page', () => {

	// const props = (propOverrides)=> Object.assign({
	// 	active: false,
	// 	children: 'All',
	// }, propOverrides)
	
	
	beforeEach(function (){
		cy.fixture("users/johnathan.json").then(user=>{
			this.johnathan = user
		})
		cy.fixture("users/unknwon.json").then(user=>{
			this.unknwon = user
		})
		this.postUrl = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCtxUdJp3oF254dfgQ8Df4ujKJFfDISBU8"
		cy.visit('/');
	})


	it('should be able to log in: johnathan', function () {
		cy.get('input[name="Email"]')
		  .type(this.johnathan.email)
		  .should("have.value", this.johnathan.email)

		cy.get('input[name="Password"]')
		  .type(this.johnathan.password)
		  .should("have.value", this.johnathan.password)

		cy.intercept(this.johnathan.postUrl).as('postUrl')

		cy.get('form')
		  .submit()

		cy.wait('@postUrl').its('response.statusCode').should('eq', 200)

	});

	it('should fail when try to log in with wrong-password', function(){
		cy.get('input[name="Email"]')
		  .type(this.johnathan.email)
		  .should("have.value", this.johnathan.email)

		cy.get('input[name="Password"]')
		  .type('123')
		  .should("have.value", '123')
		
		cy.intercept(this.postUrl).as('postUrl')
		
		cy.get('form')
		  .submit()

		cy.get('.notifications')
		  .find('div.notification')
		  .should('have.class', 'notification-type-error')
		  .find('span')
		  .should('include.text', 'Error')
		  .should('include.text', 'wrong-password')
		  
		cy.wait('@postUrl').its('response.statusCode').should('eq', 400)
		
	})

	it('should fail when try to log in with unknwon user', function(){
		cy.get('input[name="Email"]')
		  .type(this.unknwon.email)
		  .should('have.value', this.unknwon.email)
		
		cy.get('input[name="Password"]')
		  .type(this.unknwon.password)
		  .should('have.value', this.unknwon.password)
		
		cy.intercept(this.postUrl).as('postUrl')

		cy.get('form')
		  .submit()

		cy.get('.notifications')
		  .find('div.notification')
		  .should('have.class', 'notification-type-error')
		  .find('span')
		  .should('include.text', 'Error')
		  .should('include.text', 'auth/user-not-found')

		cy.wait('@postUrl').its('response.statusCode').should('eq', 400)
	})
});
