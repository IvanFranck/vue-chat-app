describe('Sign up', ()=>{

    beforeEach(function(){
        cy.fixture('users/new_user.json').then(user=>{
            this.newUser = user
        })

        cy.visit("/")
        cy.get('a[href="/signup"]').click()
        cy.get('.sign-up__title > h2').should('have.text', "Create Account")
    })

    it("Should be able to create a newuser", function(){
        cy.get('input[name="Username"')
          .type(this.newUser.username)
          .should('have.value', this.newUser.username)
        
        cy.get('input[name="Email"]')
		  .type(this.newUser.email)
		  .should('have.value', this.newUser.email)
		
		cy.get('input[name="Password"]')
		  .type(this.newUser.password)
		  .should('have.value', this.newUser.password)
    })
})